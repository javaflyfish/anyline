/*
 * Copyright 2006-2023 www.anyline.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.anyline.adapter.spring;

import org.anyline.util.BasicUtil;
import org.anyline.util.BeanUtil;
import org.anyline.util.CharUtil;
import org.anyline.util.ConfigTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

@Component("anyline.listener.EnvironmentListener")
public class SpringEnvironmentListener implements EnvironmentAware {

    @Override
    public void setEnvironment(Environment environment) {
        Field[] fields = ConfigTable.class.getDeclaredFields();
        for(Field field:fields){
            String name = field.getName();
            String value = value("anyline", environment, "." + name);
            if(BasicUtil.isNotEmpty(value)) {
                if(Modifier.isFinal(field.getModifiers())){
                    continue;
                }
                BeanUtil.setFieldValue(null, field, value);
            }
        }
    }
    /**
     * 根据配置文件提取指定key的值
     * @param prefix 前缀
     * @param environment 配置文件环境
     * @param keys key列表 第一个有值的key生效
     * @return String
     */
    public static String value(String prefix, Object environment, String ... keys){
        String value = null;
        if(null == environment || null == keys){
            return value;
        }
        Environment env = null;
        if(environment instanceof Environment){
            env = (Environment) environment;
        }else{
            return value;
        }
        if(null == prefix){
            prefix = "";
        }
        for(String key:keys){
            key = prefix + key;
            value = env.getProperty(key);
            if(null != value){
                return value;
            }
            //以中划线分隔的配置文件
            String[] ks = key.split("-");
            String sKey = null;
            for(String k:ks){
                if(null == sKey){
                    sKey = k;
                }else{
                    sKey = sKey + CharUtil.toUpperCaseHeader(k);
                }
            }
            value = env.getProperty(sKey);
            if(null != value){
                return value;
            }

            //以下划线分隔的配置文件
            ks = key.split("_");
            sKey = null;
            for(String k:ks){
                if(null == sKey){
                    sKey = k;
                }else{
                    sKey = sKey + CharUtil.toUpperCaseHeader(k);
                }
            }
            value = env.getProperty(sKey);
            if(null != value){
                return value;
            }

            ks = key.toLowerCase().split("_");
            sKey = null;
            for(String k:ks){
                if(null == sKey){
                    sKey = k;
                }else{
                    sKey = sKey + CharUtil.toUpperCaseHeader(k);
                }
            }
            value = env.getProperty(sKey);
            if(null != value){
                return value;
            }

            //中划线
            sKey = key.replace("_","-");
            value = env.getProperty(sKey);
            if(null != value){
                return value;
            }

            //小写中划线
            sKey = key.toLowerCase().replace("_","-");
            value = env.getProperty(sKey);
            if(null != value){
                return value;
            }
        }
        return value;
    }
    private static String value(Object env, String ... keys){
        return value(null, env, keys);
    }

}
