package org.anyline.adapter.spring;

import org.anyline.util.ConfigTable;
import org.anyline.util.ConfigTableAdapter;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component("anyline.config")
public class SpringConfigTableAdapter implements ConfigTableAdapter {
    private static Environment environment;
    private SpringConfigTableAdapter(){
        ConfigTable.setAdapter(this);
    }

    public static void setEnvironment(Environment environment) {
        SpringConfigTableAdapter.environment = environment;
    }

    @Override
    public Object get(String key) {
        Object val = environment.getProperty(key);
        if(null == val){
            if(key.startsWith("anyline.")){
                key = key.replace("anyline.","");
            }else{
                key = "anyline." + key;
            }
            val = environment.getProperty(key);
        }
        return val;
    }
}
