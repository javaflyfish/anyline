/*
 * Copyright 2006-2023 www.anyline.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.anyline.adapter.spring;

import org.anyline.adapter.EntityAdapter;
import org.anyline.proxy.EntityAdapterProxy;
import org.anyline.util.ConfigTable;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component("anyline.entity.adapter.spring")
public class SpringEntityAdapter {
    @Autowired(required = false)
    public void setAdapter(Map<String, EntityAdapter> adapters) {
        //是否禁用默认adapter
        String defaultKey = "anyline.entity.adapter.default";
        if(ConfigTable.IS_DISABLED_DEFAULT_ENTITY_ADAPTER ){
            adapters.remove(defaultKey);
        }
        for (EntityAdapter adapter : adapters.values()) {
            Class type = adapter.type();
            EntityAdapterProxy.reg(type, adapter);
            List<Class> types = adapter.types();
            if(null != types){
                for(Class t:types){
                    EntityAdapterProxy.reg(t, adapter);
                }
            }
        }
        for(List<EntityAdapter> list: EntityAdapterProxy.adapters.values()){
            EntityAdapter.sort(list);
        }
    }
}
