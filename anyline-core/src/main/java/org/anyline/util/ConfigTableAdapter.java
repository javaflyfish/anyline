package org.anyline.util;

public interface ConfigTableAdapter {
    Object get(String key);
}
