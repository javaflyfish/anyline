package org.anyline.data.jdbc.neo4j.entity;

import org.anyline.entity.OriginRow;

public class Neo4jRow extends OriginRow {
    public Neo4jRow() {
        primaryKeys.add("id");
        createTime = System.currentTimeMillis();
        nanoTime = System.currentTimeMillis();
    }
}